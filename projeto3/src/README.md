# Projeto 3 - Introducão à Inteligência Artificial

# Introducão

Projeto 3 da disciplina, que consiste de treinar um modelo de florestas randômicas para realizar regressão em um [dataset de selfies](https://www.crcv.ucf.edu/data/Selfie/Selfie-dataset.tar.gz), de modo a verificar quais parâmetros possuem mais importância para na classificacão de uma "boa" selfie.

# Execucão

O programa carrega um dataset e realiza regressão utilizando a biblioteca. Os parâmetros obrigatórios são

* dataset (arquivo .csv)
* número de estimadores para o algoritmo de árvores randômicas

Para executar o programa basta rodar:

```
python main.py DATASET ESTIMADORES
```

Exemplo:

```
python main.py ./selfie-dataset.csv 500
```

# Referências

**How to Take a Good Selfie**, Kalayeh, Mahdi M and Seifu, Misrak and LaLanne, Wesna and Shah, Mubarak
