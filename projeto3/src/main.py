# -*- coding: utf-8 -*-

# Projeto 3 - Introducão a Inteligência Artificial
# Autor: Matheus de Sousa Lemos Fernandes
# Matrícula: 16/0138969

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sys import argv

def readDataset(dataset):
	""" Lê o dataset passado como parâmetro e cria
	um DataFrame do pandas

	Arguments:
		dataset {string} -- Nome do dataset

	Returns:
		pandas.core.frame.DataFrame -- DataFrame do pandas com os dados do dataset
	"""

	features = pd.read_csv(dataset)
	return features

def convertToArray(features):
	""" Converte o DataFrame para um array do numpy

	Arguments:
		features {pandas.core.frame.DataFrame} -- DataFrame do pandas

	Returns:
		features {np.array} -- Array das features
		labels {np.array} -- Array das labels
		featureList {list[]} -- Lista com o nome das colunas
	"""

	labels = np.array(features['popularity_score'])
	features = features.drop('popularity_score', axis=1)
	features = features.drop('image_name', axis=1)

	# Salva os nomes das features
	featureList = list(features.columns)
	features = np.array(features)
	return features, labels, featureList


def trainModel(trainFeatures, testFeatures, trainLabels, testLabels, featureList, estimators):
	""" Treina o modelo e tira métricas

	Arguments:
		trainFeatures {np.array} -- Array de features de treino
		testFeatures {np.array} -- Array de features de teste
		trainLabels {np.array} -- Array de labels de treino
		testLabels {np.array} -- Array de labels de teste
	"""

	# Treina o modelo com seed 42
	randomForest = RandomForestRegressor(n_estimators=estimators, random_state=42, bootstrap=True)
	randomForest.fit(trainFeatures, trainLabels)

	# Faz predicões em cima do modelo treinado
	predictions = randomForest.predict(testFeatures)
	errors = abs(predictions - testLabels)

	# Obtém métricas para o treinamento usando erro percentual médio (MAPE)
	mape = 100 * (errors / testLabels)

	# Acurácia e score
	accuracy = 100 - np.mean(mape)

	print("\nResults:")
	print('Accuracy:', round(accuracy, 2), '%.')

	# Importância de cada feature
	featureImportance = randomForest.feature_importances_
	getFeatureImportance(featureList, featureImportance)


def getFeatureImportance(featureList, featureImportance):
	""" Pega a importância de cada parâmetro

	Arguments:
		featureList {list} -- Lista com nome das features
		featureImportance {np.array} -- Array com a importância de cada feature
	"""

	array = []
	for i in range(0, len(featureList) - 1):
		element = []
		element.append(featureList[i])
		element.append(featureImportance[i])
		array.append(tuple(element))

	sortedArray = sorted(array, key=lambda element: element[1], reverse=True)
	print("\nFeature importance:\n")

	for element in sortedArray:
		print("%s: %.4f" %(element[0], element[1]))

def main(params):
	""" Funcão principal
	"""

	# Parâmetros
	dataset = params[1]
	estimators = int(params[2])

	# Busca o dataset e formata em arrays
	print("Loading dataset")
	features = readDataset(dataset)
	arrayFeatures, labels, featureList = convertToArray(features)


	# Separa entre dados de teste e treino
	print("Training model")
	trainFeatures, testFeatures, trainLabels, testLabels = train_test_split(arrayFeatures, labels, test_size=0.33, random_state=42)
	trainModel(trainFeatures, testFeatures, trainLabels, testLabels, featureList, estimators)

main(argv)
