from keras.models import Sequential
from keras.applications.vgg16 import VGG16, preprocess_input, decode_predictions
from keras.layers import Flatten, Dense, Dropout
from keras.layers import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.utils import to_categorical
from keras.preprocessing.image import load_img, ImageDataGenerator, img_to_array
from keras.preprocessing import image
from sys import argv
import numpy as np


# Model
def createCustomModel():
	model = Sequential()
	model.add(Convolution2D(32, 3, 3, activation='relu', input_shape=(64, 64, 3)))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Convolution2D(64, 3, 3, activation='relu'))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Flatten())
	model.add(Dense(output_dim=64, activation='relu'))
	model.add(Dropout(0.5))
	model.add(Dense(output_dim=3, activation='softmax'))

	print(model.summary())
	model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
	return model


# Training
def trainModel():
	# Create model
	x_train = np.random.random((100, 100, 3))
	y_train = to_categorical(np.random.randint(10, size=(100, 1)), num_classes=10)
	x_test = np.random.random((20, 100, 3))
	y_test = to_categorical(np.random.randint(10, size=(20, 1)), num_classes=10)
	model = createCustomModel()

	# Data augmentation with ImageDataGenerator
	train_datagen = ImageDataGenerator(
		rescale=1./255,
		shear_range=0.2,
		zoom_range=0.2,
		horizontal_flip=True
	)
	test_datagen = ImageDataGenerator(rescale=1./255)

	# Load images
	training_set = train_datagen.flow_from_directory(
		directory='images/train',
		target_size=(64, 64),
		batch_size=32,
		class_mode='categorical' 
	)

	test_set = train_datagen.flow_from_directory(
		directory='images/test',
		target_size=(64, 64),
		batch_size=32,
		class_mode='categorical' 
	)

	model.fit_generator(
		training_set,
		steps_per_epoch=10,
		epochs=10,
		validation_data=test_set,
		validation_steps=100
	)
	
	model.save('custom_model.h5')

	# Test with chosen image
	return model

def predictImage(img):
	test_image = image.load_img(img, target_size=(64, 64))
	test_image = image.img_to_array(test_image)
	test_image = np.expand_dims(test_image, axis=0)

	model = createCustomModel()
	model.load_weights('custom_model.h5')
	result = model.predict(test_image)
	print(result)

def createVGG16Model():
	model = Sequential()
	model.add(Convolution2D(64, (3, 3), activation='relu', input_shape=(226, 226, 3)))
	model.add(Convolution2D(64, (3, 3), activation='relu', padding='same'))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Convolution2D(128, (3, 3), activation='relu', padding='same'))
	model.add(Convolution2D(128, (3, 3), activation='relu', padding='same'))
	model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
	model.add(Convolution2D(256, (3, 3), activation='relu', padding='same'))
	model.add(Convolution2D(256, (3, 3), activation='relu', padding='same'))
	model.add(Convolution2D(256, (3, 3), activation='relu', padding='same'))
	model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
	model.add(Convolution2D(512, (3, 3), activation='relu', padding='same'))
	model.add(Convolution2D(512, (3, 3), activation='relu', padding='same'))
	model.add(Convolution2D(512, (3, 3), activation='relu', padding='same'))
	model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
	model.add(Convolution2D(512, (3, 3), activation='relu', padding='same'))
	model.add(Convolution2D(512, (3, 3), activation='relu', padding='same'))
	model.add(Convolution2D(512, (3, 3), activation='relu', padding='same'))
	model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
	model.add(Flatten())
	model.add(Dense(4096, activation='relu'))
	model.add(Dense(4096, activation='relu'))
	model.add(Dense(3, activation='softmax'))

	print(model.summary())
	model.load_weights('model.h5')
	return model

def main(createModel, train, img):
	print(createModel)
	if createModel == 'True':
		if train == '1':
			model = trainModel()
			predictImage(img)
		else:
			predictImage(img)
	else:
		model = createVGG16Model()
		test_image = image.load_img(img, target_size=(226, 226))
		test_image = img_to_array(test_image)
		test_image = test_image.reshape(1, test_image.shape[0], test_image.shape[1], test_image.shape[2])
		test_image = preprocess_input(test_image)
		result = model.predict(test_image)
		print(result)

img = 'images/validation/selfie9.jpg'
main(argv[1], argv[2], img)
