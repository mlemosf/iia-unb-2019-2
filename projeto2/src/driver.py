#!/usr/local/bin/python
# -*- coding: utf-8 -*-

# Projeto 2 - Introducão a Inteligência Artificial - UnB - 2019
# Autor: Matheus de Sousa Lemos Fernandes
# Matrícula: 16/0137969

from pyke import knowledge_engine

def proveSymptoms(dictSintomas):
	e = knowledge_engine.engine(__file__)
	e.activate('sintomas')

	print "\nPROVA\n"
	with e.prove_goal('sintomas.get_sintomas($sintomas)',sintomas=dictSintomas) as gen:
		for v, p in gen:
			print "%s" % (p)

def validateInputs(sintomas):

	erros = []
	if sintomas['febre'] not in ['A', 'B', 'C']:
		erros.append('Valor da febre deve ser A, B ou C')
	if sintomas['manchas'] not in ['A','B','C']:
		erros.append("'Valor de 'manchas na pele' deve ser A, B ou C")
	if sintomas['dor_muscular'] not in ['A','B','C']:	
		erros.append("Valor de 'dor nos musculos' deve ser A, B ou C")
	if sintomas['dor_articulacao'] not in ['A','B','C']:
		erros.append("Valor de 'dor nas articulacoes' deve ser A, B ou C")
	if sintomas['intensidade_dor'] not in ['A','B','C']:
		erros.append("Valor de 'intensidade da dor' deve ser A, B ou C")
	if sintomas['edema'] not in ['A','B','C']:
		erros.append("Valor de 'dema da articulacão' deve ser A, B ou C")
	if sintomas['conjuntivite'] not in ['A', 'B', 'C']:
		erros.append("Valor de 'conjuntivite' deve ser A, B ou C")
	if sintomas['dor_de_cabeca'] not in ['A','B']:
		erros.append("Valor de 'dor de cabeca' deve ser A ou B")
	if sintomas['coceira'] not in ['A','B']:
		erros.append("Valor de 'coceira' deve ser A ou B")
	if sintomas['hipertrofia'] not in ['A','B','C']:
		erros.append("Valor de 'hipertrofia ganglionar' deve ser A, B ou C")
	if sintomas['discrasia'] not in ['A','B','C']:	
		erros.append("Valor de 'discrasia hemorragica' deve ser A, B ou C")
	if sintomas['acometimento'] not in ['A','B']:	
		erros.append("Valor de 'acomentimento neurológico' deve ser A ou B")

	if erros:
		raise Exception(erros)
	else:
		return erros

def getInputs():

	# Variaveis
	sintomas = {}

	print "Digite valores para os sintomas a seguir\n"
	while True:
		try:
			sintomas['febre'] = str(raw_input("Febre (duracão) - A(1-2 dias), B(2-3 dias), C(4-7 dias): "))
			sintomas['manchas'] = str(raw_input("Manchas na pele - A(1-2o dia),B(2-5o dia), C(4o dia): "))
			sintomas['dor_muscular'] = str(raw_input("Dor nos músculos - A(+), B(++), C(+++): "))
			sintomas['dor_articulacao'] = str(raw_input("Dor nas articulacões - A(+), B(++), C(+++): "))
			sintomas['intensidade_dor'] = str(raw_input("Intensidade da dor articular - A(leve), B(moderada), C(intensa): "))
			sintomas['edema'] = str(raw_input("Edema da articulacão - A(raro), B(frequente e de leve intensidade), C(frequente e de alta intensidade): "))
			sintomas['conjuntivite'] = str(raw_input("Conjuntivite - A(raro), B(50-90%), C(30%): "))
			sintomas['dor_de_cabeca'] = str(raw_input("Dor de cabeca - A(++), B(+++): "))
			sintomas['coceira'] = str(raw_input("Coceira - A(leve), B(moderada/intensa): "))
			sintomas['hipertrofia'] = str(raw_input("Hipertrofia ganglionar - A(leve), B(moderada), C(intensa): "))
			sintomas['discrasia'] = str(raw_input("Discrasia hemorragica - A(ausente), B(leve), C(moderada): "))
			sintomas['acometimento'] = str(raw_input("Acomentimento neurologico - A(raro), B(mediano): "))
		except ValueError:
			print "Valor inválido"
			continue
		else:
			break

	return sintomas

def inputValues():
	try:
		sintomas = getInputs()
		sintomasValidos = validateInputs(sintomas)		
	except Exception as e:
		for error in list(e[0]):
			print "ERRO: " + error
	return sintomas

def proveValues(sintomas):
	try:
		proveSymptoms(sintomas)
	except Exception as e:
		print str(e)

def main():
	sintomas = inputValues()
	proveValues(sintomas)

main()