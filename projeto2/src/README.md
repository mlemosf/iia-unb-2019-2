# Projeto 2 - Introducão a Inteligência Artificial

**Autor**: Matheus de Sousa Lemos Fernandes  
**Matrícula**: 16/0137969  

## Descricão

Criacão de um sistema especialista utilizando o programa PyKE.  
O sistema visa diagnosticar com sucesso 3 doencas: Dengue, zika e chikungunya, por meio de regras de producão e uma máquina de inferência.

## Execucão do programa

Para executar o programa é necessário ter instalado o python2.
A execucão é feita na pasta base do projeto, utilizando:

```
python2 driver.py
```

O programa é interativo, e cada input deve receber apenas algum dos inputs fornecidos para escolha (inputs fora das opcões de escolha serão invalidados em uma etapa de verificacão), e são sempre letras maíusculas **A**, **B** ou **C**.  
Após o input e validacão, a prova é realizada, dando, por fim, um diagnóstico final.

## Casos funcionais

O programa foi testado e pôde-se verificar que o programa realiza o diagnóstico correto para os seguintes inputs:

**Dengue**: C,C,C,A,A,A,A,B,A,A,C,A  
**Zika**: A,A,B,B,B,B,B,A,B,C,A,B  
**Chikungunya**: B,B,A,C,C,C,C,A,A,B,B,A  

## Referências

[PyKE - Python Knowledge Engine](http://pyke.sourceforge.net/)