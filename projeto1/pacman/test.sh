#!/bin/bash

# Testa o jogo para todas as combinacões de mapas e algoritmos

algorithms=(aStarSearch)
maps=(tinyMaze smallMaze mediumMaze bigMaze openMaze)
h=(euclideanHeuristic manhattanHeuristic breakTiesHeuristic)

for h in "${h[@]}"
do
	for m in "${maps[@]}"
	do
		#printf "%s\n" "$a"
		python pacman.py -p SearchAgent -a fn=aStarSearch,heuristic=${h} --layout $m
	done
done	
