# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
# from util import Stack, PriorityQueue
from game import Directions
from time import sleep
from math import sqrt, pow

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """
        

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]


# ===== DEPTH FIRST SEARCH (DFS)

# Variáveis        
visited = []
pilha = util.Stack()
path = []
n = Directions.NORTH
s = Directions.SOUTH
e = Directions.EAST
w = Directions.WEST

def getNotVisited(problem, node, visited):
    """ Busca os vizinhos não visitados de cada nó
    de modo a evitar expansão desnecessária

    Arguments:
        problem {problem} -- Referência do problem
        node {tuple} -- Posicão do nó (x,y)

    Returns:
        [tuple[]] -- Lista de tuplas com posicões não visitadas
    """

    notVisited = []
    neighbors = problem.getSuccessors(node)
    for n in neighbors:
        if n[0] not in visited:
            notVisited.append(n)

    return notVisited

def getPreviousDirection(direction):
    """ Pega a direcão de onde o pacman veio

    Arguments:
        direction {str} -- Direcão para onde o pacman se moveu

    Returns:
        [str] -- Direcão de onde o pacman veio
    """

    if direction == s:
        stack = n
    elif direction == n:
        stack = s
    elif direction == e:
        stack = w
    elif direction == w:
        stack = e
    return stack

def depthFirstSearchUtil(problem, node):
    """ Executa o algoritmo de busca em profundidade
    (depth first search) de forma recursiva e retorna uma
    lista com as direcões a serem seguidas.

    Arguments:
        goalPosition (tuple) -- Posicão que se deseja alcancar
        node {tuple} -- Posicão no mapa (x,y)
    """

    visited.append(node)
    notVisited = getNotVisited(problem, node, visited)
    for neighbor in notVisited:
        if neighbor[0] not in visited:
            path.append(neighbor[1])
            pilha.push(getPreviousDirection(neighbor[1]))
            depthFirstSearchUtil(problem, neighbor[0])
            if not pilha.isEmpty():
                path.append(pilha.pop())
    return path

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """

    finalPath = depthFirstSearchUtil(problem, problem.getStartState())
    return finalPath

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

# === A* ALGORITHM === #

n = Directions.NORTH
s = Directions.SOUTH
e = Directions.EAST
w = Directions.WEST

def getDirection(src, dst):
    """ Busca direcão baseado no movimento de uma posicão à outra

    Arguments:
        src {tuple} -- Posicão de origem
        dst {tuple} -- Posicão de destino

    Returns:
        str -- Direcão do movimento
    """

    if src[0] < dst[0]:
        direction = w
    if src[0] > dst[0]:
        direction = e
    if src[1] < dst[1]:
        direction = s
    if src[1] > dst[1]:
        direction = n
    return direction

def buildPath(cameFrom, current):
    """ Constrói o caminho a partir do dicionário contruído pelo A*

    Arguments:
        cameFrom {dict} -- Dicionário com 'destino: origem'
        current {tuple} -- Tupla com a posicão inicial do caminho

    Returns:
        list -- Lista com o caminho do pacman
    """

    path.append(getDirection(current, cameFrom[current]))
    while current in cameFrom.keys():
        if cameFrom[current] is not None:
            path.insert(0, getDirection(current, cameFrom[current]))
        current = cameFrom[current]

    return path


def aStarSearchUtil(problem, goalPosition, heuristic):
    """ Funcão principal do algoritmo de busca A*

    Arguments:
        problem {problem} -- Referência ao problem
        goalPosition {tuple} -- Posicão de destino
        heuristic {function} -- Heurística a ser utilizada pelo algoritmo

    Returns:
        dict -- Dicionário com as posicões e seus respectivos 'pais'
    """

    # Variáveis
    frontier = util.PriorityQueue()
    start = problem.getStartState()
    cameFrom = {}
    costSoFar = {}
    cameFrom[start] = None
    costSoFar[start] = 0

    frontier.push(start, 0)

    while not frontier.isEmpty():
        current = frontier.pop()
        if current == goalPosition:
            break

        for neighbor in problem.getSuccessors(current):
            newCost = costSoFar[current] + neighbor[2]
            if neighbor[0] not in costSoFar or newCost < costSoFar[neighbor[0]]:
                costSoFar[neighbor[0]] = newCost
                priority = newCost + heuristic(neighbor[0], problem, start)
                frontier.push(neighbor[0], priority)
                cameFrom[neighbor[0]] = current
    return cameFrom

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""

    ## Variáveis
    goalPosition = (1, 1)
    path = aStarSearchUtil(problem, goalPosition, heuristic)
    finalPath = buildPath(path, goalPosition)
    return finalPath


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
